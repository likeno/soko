# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Validation for the configuration."""

import collections
import logging
import string

LOGGER = logging.getLogger(__name__)


def validate_resource_settings(settings):
    """Validate settings

    Parameters
    ----------
    settings: dict
        A mapping with the settings to validate
    """

    validator = ResourceSettingsValidator()
    for name, resource_config in settings["resources"].items():
        LOGGER.debug("Validating {!r}...".format(name))
        validator(name, resource_config)


class ResourceSettingsValidator(object):
    MANDATORY_KEYS = [
        "input_search_url_patterns",
    ]
    OPTIONAL_KEYS = [
        "alternatives",
        "output_search_url_patterns",
        "repetitions",
        "name_patterns",
        "uri_pattern",
    ]

    def __call__(self, resource_name, resource_config):
        validate_identifier(resource_name)
        self.validate_allowed_resource_config_keys(resource_config)
        self.validate_mandatory_resource_config_keys(resource_config)
        self.validate_keyed_config("repetitions", resource_config)
        self.validate_keyed_config("alternatives", resource_config)
        self.validate_list_config("name_patterns", resource_config)
        self.validate_list_config("search_url_patterns", resource_config)

    def validate_mandatory_resource_config_keys(self, resource_config):
        for key in self.MANDATORY_KEYS:
            if key not in resource_config.keys():
                raise RuntimeError(
                    "Invalid resource configuration: Missing {!r}".format(key)
                )

    def validate_allowed_resource_config_keys(self, resource_config):
        all_allowed = self.MANDATORY_KEYS + self.OPTIONAL_KEYS
        for key in resource_config.keys():
            if key not in all_allowed:
                raise RuntimeError(
                    "Invalid resource configuration: "
                    "Illegal key {!r}".format(key)
                )

    def validate_keyed_config(self, key, resource_config):
        keyed_config = resource_config.get(key, dict())
        for keyed_key, keyed_value in keyed_config.items():
            validate_identifier(keyed_key)
            self.validate_list_config(keyed_key, keyed_config)

    def validate_list_config(self, key, resource_config):
        list_config = resource_config.get(key, [])
        if not isinstance(list_config, collections.Iterable):
            raise RuntimeError(
                "Invalid resource configuration: {!r} must be "
                "a list".format(list_config)
            )


def validate_identifier(identifier):
    allowed_first_characters = string.ascii_lowercase + "_"
    allowed_rest_characters = allowed_first_characters + string.digits
    if identifier[0] not in allowed_first_characters:
        raise RuntimeError(
            "Invalid Identifier: Character {!r} is not "
            "allowed at the start of an identifier".format(identifier[0])
        )
    for char in identifier:
        if char not in allowed_rest_characters:
            raise RuntimeError(
                "Invalid Identifier: Character {!r} is not "
                "allowed".format(char)
            )
