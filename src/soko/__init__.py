import pkg_resources

__version__ = pkg_resources.require("soko")[0].version
