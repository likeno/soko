# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Handlers for URLs using the `http` scheme"""

from future.standard_library import install_aliases
install_aliases()

# the following comments that start with noqa instruct flake8 to ignore the
# import error that is caused by using the `future` module in order to
# provide code compatible with python2
import logging  # noqa: E402
import os  # noqa: E402
from urllib.parse import urlparse  # noqa: E402

import exrex  # noqa: E402
import requests  # noqa: E402

logger = logging.getLogger(__name__)


# TODO: Add a maximum download size
# http://docs.python-requests.org/en/latest/user/advanced/#body-content-workflow
# TODO: Report download progress
def get_resource(url, destination=None, unfold_url_regexes=False, **kwargs):
    """Fetch a URL from the remote host.

    The paths are copied to localHost's destination.

    Parameters
    ----------
    url: str
        sftp URL with the address of the path to get from the remote host
    destination: str
        Path to a local directory where the resource returned by the URL is
        to be copied to.
    unfold_url_regexes: bool, optional
        Whether to treat the input ``url`` parameter as a regular expression
        or not. If ``True``, the url will be unfolded into multiple urls by
        generating the strings that match the regular expression defined
        in the ``url``. then these will be searched for and the first that
        exists is then used as the proper URL to be fetched. When using this
        parameter ordinary URL characters that might have a special meaning
        as a regular expression **must** be properly escaped by prefixing them
        with a backslash.

    Returns
    -------
    str
        Local path to the newly fetched file

    """

    if unfold_url_regexes:
        real_url = list_resource(url, unfold_url_regexes=True, **kwargs)
    else:
        real_url = url
    leaf_url = real_url[:-1] if real_url.endswith("/") else real_url
    download_filename = urlparse(leaf_url).path.rpartition("/")[-1]
    if destination is None:
        local_path = os.path.join(os.getcwd(), download_filename)
    elif os.path.isdir(destination):
        local_path = os.path.join(destination, download_filename)
    else:
        local_path = destination
    logger.debug("Streaming download of {!r} to "
                 "{!r}...".format(leaf_url, local_path))
    with requests.get(leaf_url, stream=True) as response, \
            open(local_path, "wb") as fh:
        for chunk in response.iter_content(chunk_size=1024):
            fh.write(chunk)
    return local_path


def list_resource(url, unfold_url_regexes=False, **kwargs):
    """Find paths on the remote host.

    Parameters
    ----------
    url: str
        HTTP URL with the address of a pattern representing the paths to be
        found.
    unfold_url_regexes: bool, optional
        Whether to treat the input ``url`` parameter as a regular expression
        or not. If ``True``, the url will be unfolded into multiple urls by
        generating the strings that match the regular expression defined
        in the ``url``. When using this parameter ordinary URL characters that
        might have a special meaning as a regular expression **must** be
        properly escaped by prefixing them with a backslash.

    Returns
    -------
    str
        URL that matches the input pattern described by the input URL.

    """

    if unfold_url_regexes:
        for unfolded_url in sorted(exrex.generate(url)):
            found = _list_single_url(unfolded_url, **kwargs)
            if found is not None:
                result = found
                break
        else:
            result = None
    else:
        result = _list_single_url(url, **kwargs)
    return result


def put_resource(path, url, **kwargs):
    """Send the local path to the remote server.

    Parameters
    ----------
    path: str
        Path in the local file system to be sent to the remote. It is
        assumed to contain the full path to a file and not a search
        pattern.
    url: str
        HTTP URL representing the a directory on the remote server where the
        path will be POSTed.

    Returns
    -------
    bool
        Overall result of the operation.

    """

    raise NotImplementedError


def _list_single_url(url, **kwargs):
    """Find paths on the remote host.

    Parameters
    ----------
    url: str
        HTTP URL with the address of a pattern representing the paths to be
        found.

    Returns
    -------
    str
        URL that matches the input pattern described by the input URL.

    """

    response = requests.head(url)
    try:
        response.raise_for_status()
    except requests.HTTPError:
        result = None
    else:
        result = url
    return result
