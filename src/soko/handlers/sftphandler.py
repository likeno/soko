# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""handlers for URLs using the `sftp` scheme.

This module assumes that ssh is configured with public key authentication and
that there is a properly configured openssh config file.

"""

from future.standard_library import install_aliases
install_aliases()

# the following comments that start with noqa instruct flake8 to ignore the
# import error that is caused by using the `future` module in order to
# provide code compatible with python2
from itertools import count  # noqa: E402
import logging  # noqa: E402
import os  # noqa: E402
import re  # noqa: E402
import subprocess  # noqa: E402
from urllib.parse import urlparse  # noqa: E402
from time import sleep  # noqa: E402

from backports.functools_lru_cache import lru_cache  # noqa: E402
import paramiko  # noqa: E402

logger = logging.getLogger(__name__)


def get_resource(url, destination=None, openssh_config_path="~/.ssh/config",
                 **kwargs):
    """Fetch a URL from the remote host.

    The paths are copied to localHost's destination.

    Parameters
    ----------
    url: str
        sftp URL with the address of the path to get from the remote host
    destination: str
        Path to a local directory where the resource returned by the URL is
        to be copied to.
    openssh_config_path: str, optional
        Path to the openssh configuration file to use

    Returns
    -------
    str
        Local path to the newly fetched file

    """

    found = list_resource(url, openssh_config_path=openssh_config_path,
                          **kwargs)
    if found is None:
        full_destination = None
    else:
        destination = destination or os.getcwd()
        if not os.path.isdir(destination):
            try:
                os.makedirs(destination)
            except OSError as err:
                if err.args[0] == 17:  # the directory already exists
                    pass
        parsed_url = urlparse(found)
        fname = os.path.basename(parsed_url.path)
        full_destination = os.path.join(destination, fname)
        ssh_client = _get_ssh_client(parsed_url.hostname, openssh_config_path)
        with _get_sftp_client(ssh_client) as sftp_client:
            paramiko.agent.AgentRequestHandler(sftp_client)
            sftp_client.get(parsed_url.path, full_destination)
    return full_destination


def list_resource(url, openssh_config_path="~/.ssh/config", **kwargs):
    """Find paths on the remote host.

    Parameters
    ----------
    url: str
        sftp URL with the address of a pattern representing the paths to be
        found. This pattern is interpreted as a regular expression.
    openssh_config_path: str, optional
        Path to the openssh configuration file to use

    Returns
    -------
    str
        URL that matches the input pattern described by the input URL.
    """

    parsed_url = urlparse(url)
    ssh_client = _get_ssh_client(parsed_url.hostname, openssh_config_path)
    directory_path, file_name_pattern = os.path.split(parsed_url.path)
    pattern = re.compile(file_name_pattern)
    with _get_sftp_client(ssh_client) as sftp_client:
        paramiko.agent.AgentRequestHandler(sftp_client)
        for item in sftp_client.listdir(directory_path):
            if pattern.search(item) is not None:
                found_path = os.path.join(directory_path, item)
                found_url = "sftp://{netloc}{path}".format(
                    netloc=parsed_url.netloc, path=found_path)
                break
        else:
            found_url = None
    return found_url


def put_resource(path, url, openssh_config_path="~/.ssh/config", **kwargs):
    """Send the local path to the remote server.

    Parameters
    ----------
    path: str
        Path in the local file system to be sent to the remote. It is
        assumed to contain the full path to a file and not a search
        pattern.
    url: str
        sftp URL representing the a directory on the remote server where the
        path will be put. The directory tree will be created in case it
        doesn't exist.
    openssh_config_path: str, optional
        Path to the openssh configuration file to use

    Returns
    -------
    bool
        Overall result of the operation.

    """

    parsed_url = urlparse(url)
    destination = parsed_url.path
    destination_dir = os.path.dirname(destination)
    ssh_client = _get_ssh_client(parsed_url.hostname, openssh_config_path)
    _create_remote_dirs(destination_dir, ssh_client)
    with _get_sftp_client(ssh_client) as sftp_client:
        logger.debug(
            "Sending {!r} to {!r}...".format(
                path, os.path.join(path, destination))
        )
        put_result = sftp_client.put(
            path,
            destination
        )
    return put_result


def _get_sftp_client(ssh_client):
    """Return a paramiko.SFTPClient"""
    channel = ssh_client.get_transport().open_session()
    if _test_for_ssh_agent():
        paramiko.agent.AgentRequestHandler(channel)
    channel.invoke_subsystem("sftp")
    sftp_client = paramiko.sftp_client.SFTPClient(channel)
    return sftp_client


def _create_remote_dirs(path, ssh_client):
    channel = ssh_client.get_transport().open_session()
    if _test_for_ssh_agent():
        paramiko.agent.AgentRequestHandler(channel)
    exit_code, stdout, stderr = _run_command(
        channel,
        "mkdir -p {}".format(path)
    )
    if exit_code != 0:
        raise RuntimeError(stderr)


@lru_cache()
def _get_ssh_client(host, openssh_config_path="~/.ssh/config", timeout=20):
    """Create an ssh client that can communicate with the input host.

    This function assumes that the ssh connection has already been previously
    configured by creating an openssh config file. It also assumes that the
    ssh connection is established using key-based authentication. It is
    capable of using ssh-agent in order to forward credentials checking too.
    An example
    of an ssh config file would be:

    Host somehost
        User someuser
        IdentityFile /path/to/identity/file

    Parameters
    ----------
    host: str
        IP or host name of the proxy machine as configured in the supplied
        openssh configuration file
    openssh_config_path: str, optional
        Path to the openssh configuration file to use
    timeout: int, optional
        Timeout in seconds for establishing the ssh connection

    Returns
    -------
    paramiko.SSHClient
        The ssh client object

    """
    proxy, host_info = _get_ssh_proxy(host, openssh_config_path)
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    connect_kwargs = {
        "username": host_info["user"],
        "sock": proxy,
        "timeout": timeout
    }
    if not _test_for_ssh_agent():
        connect_kwargs["key_filename"] = host_info.get("identityfile")
    client.connect(host_info["hostname"], **connect_kwargs)
    return client


def _get_ssh_proxy(host, openssh_config_path="~/.ssh/config"):
    """Generate an appropriate ssh proxy for tunneling ssh connections.

    This function is able to use the config files used by OpenSSH and that
    are usually used to defined ssh proxies.

    Parameters
    ----------
    host: str
        IP or host name of the proxy machine as configured in the supplied
        openssh configuration file
    openssh_config_path: str, optional
        Path to the openssh configuration file to use

    Returns
    -------
    paramiko.ProxyCommand
        The configured proxy object that can be used to setup the tunneled
        sftp connection
    dict
        Configuration values for the input host name that have been read from
        the openssh config file

    """

    config = paramiko.SSHConfig()
    config.parse(open(os.path.expanduser(openssh_config_path)))
    host_config = _find_host_config(host, config)
    if "proxycommand" in host_config:
        default_shell = os.getenv("SHELL", "/bin/sh")
        proxy_command = host_config["proxycommand"]
        proxy = paramiko.ProxyCommand(
            subprocess.check_output([
                default_shell,
                "-c",
                "echo {}".format(proxy_command)
            ]).strip()
        )
    else:
        proxy = None
    return proxy, host_config


def _find_host_config(host_name, openssh_config):
    """Find the configuration for the specified host in openssh config file.

    This function looks through the openssh configuration file and tries to
    find a host whose name matches the `host_name` parameter. Name matching
    is done based on `Host` and also on `HostName` entries. This is useful
    for allowing hosts to be assigned nicknames in the openssh config file.

    Parameters
    ----------
    host_name: str
        IP or host name of the host to find ocnfiguration for. This is
        searched for both in `Host` and in `HostName` entries in the openssh
        config file
    openssh_config: paramiko.config.SSHConfig
        The already loaded ssh configuration

    Returns
    -------
    dict
        A mapping with the configuration parameters for the input host

    """

    available_hostnames = openssh_config.get_hostnames()
    if host_name in available_hostnames:
        host_config = openssh_config.lookup(host_name)
    else:
        for host_nickname in openssh_config.get_hostnames():
            host_config = openssh_config.lookup(host_nickname)
            if host_name == host_config["hostname"]:
                break
        else:
            host_config = {}
    return host_config


def _run_command(channel, command):
    """Run a command over an SSH channel"""
    channel.exec_command(command)
    return _wait_for_data(channel)


def _test_for_ssh_agent():
    """Check if an ssh-agent is running

    This is adapted from :

    https://gist.github.com/toejough/436540622530c35404e6#gistcomment-1479818

    """
    agent_keys = paramiko.Agent().get_keys()
    return True if len(agent_keys) > 0 else False


def _wait_for_data(channel, chunksize=1024):
    """Wait for data sent by a running command on the input SSH channel.

    Parameters
    ----------
    channel: paramiko.Channel
        A channel where a command has been run
    chunksize: int, optional
        The number of bytes to retrieve frm the channel during each iteration

    Returns
    -------
    status_code: int
        The command's exit code
    stdout: str
        Text sent by the command to the channel's standard output stream
    stderr: str
        Text sent by the command to the channel's standard error stream

    """

    stdout = ""
    stderr = ""
    stdout_done = False
    stderr_done = False
    for _ in count():
        exited = channel.exit_status_ready()
        if not stdout_done:
            if not channel.recv_ready() and not exited:
                sleep(0.2)
            else:
                out_chunk = channel.recv(chunksize)
                out_buf_full = True if len(out_chunk) == chunksize else False
                if len(out_chunk) > 0:
                    stdout += out_chunk
                if not out_buf_full or len(out_chunk) == 0:
                    # reached end of output
                    stdout_done = True
        if not stderr_done:
            if not channel.recv_stderr_ready() and not exited:
                sleep(0.2)
            else:
                err_chunk = channel.recv_stderr(chunksize)
                err_buf_full = True if len(err_chunk) == chunksize else False
                if len(err_chunk) > 0:
                    stderr += err_chunk
                if not err_buf_full or len(err_chunk) == 0:
                    # reached end of output
                    stderr_done = True
        if stdout_done and stderr_done:
            break
    status_code = channel.recv_exit_status()
    return status_code, stdout, stderr
