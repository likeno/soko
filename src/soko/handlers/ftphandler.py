# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""handlers for URLs using the `ftp` scheme.

soko expects FTP credentials to be defined in a netrc file

"""

from future.standard_library import install_aliases
install_aliases()

# the following comments that start with noqa instruct flake8 to ignore the
# import error that is caused by using the `future` module in order to
# provide code compatible with python2
from contextlib import contextmanager  # noqa: E402
import logging  # noqa: E402
from netrc import netrc  # noqa: E402
import os  # noqa: E402
import re  # noqa: E402
from urllib.parse import urlparse  # noqa: E402

import ftputil  # noqa: E402
from ftputil.session import session_factory  # noqa: E402

logger = logging.getLogger(__name__)


def get_resource(url, destination=None, use_passive_mode=False,
                 netrc_path=None, **kwargs):
    """Fetch a URL from the remote host.

    The paths are copied to localHost's destination.

    Parameters
    ----------
    url: str
        sftp URL with the address of the path to get from the remote host
    destination: str
        Path to a local directory where the resource returned by the URL is
        to be copied to.
    use_passive_mode: bool, optional
        Whether to use FTP PASSIVE mode.
    netrc_path: str, optional
        Full path to the netrc file that has the configuration for logging in
        to the designated host. Defaults to ``~/.netrc``

    Returns
    -------
    str
        Local path to the newly fetched file

    """

    found = list_resource(url, use_passive_mode=use_passive_mode,
                          netrc_path=netrc_path, **kwargs)
    if found is None:
        full_destination = None
    else:
        destination = destination or os.getcwd()
        if not os.path.isdir(destination):
            try:
                os.makedirs(destination)
            except OSError as err:
                if err.args[0] == 17:  # the directory already exists
                    pass
        parsed_url = urlparse(found)
        fname = os.path.basename(parsed_url.path)
        full_destination = os.path.join(destination, fname)
        with get_ftp_connection(parsed_url.hostname,
                                port=parsed_url.port or 21,
                                use_passive_mode=use_passive_mode,
                                netrc_path=netrc_path) as ftp_host:
            ftp_host.download(parsed_url.path, full_destination)
    return full_destination


def list_resource(url, use_passive_mode=False, netrc_path=None, **kwargs):
    """Find paths on the remote host.

    Parameters
    ----------
    url: str
        sftp URL with the address of a pattern representing the paths to be
        found. This pattern is interpreted as a regular expression.
    use_passive_mode: bool, optional
        Whether to use FTP PASSIVE mode.
    netrc_path: str, optional
        Full path to the netrc file that has the configuration for logging in
        to the designated host. Defaults to ``~/.netrc``

    Returns
    -------
    str
        URL that matches the input pattern described by the input URL.

    """

    parsed_url = urlparse(url)
    directory_path, file_name_pattern = os.path.split(parsed_url.path)
    pattern = re.compile(file_name_pattern)
    with get_ftp_connection(parsed_url.hostname, port=parsed_url.port or 21,
                            use_passive_mode=use_passive_mode,
                            netrc_path=netrc_path) as ftp_host:
        for item in ftp_host.listdir(directory_path):
            if pattern.search(item) is not None:
                found_path = os.path.join(directory_path, item)
                found_url = "ftp://{netloc}{path}".format(
                    netloc=parsed_url.netloc, path=found_path)
                break
        else:
            found_url = None
    return found_url


def put_resource(path, url, use_passive_mode=False, netrc_path=None, **kwargs):
    """Send the local path to the remote server.

    Parameters
    ----------
    path: str
        Path in the local file system to be sent to the remote. It is
        assumed to contain the full path to a file and not a search
        pattern.
    url: str
        sftp URL representing the a directory on the remote server where the
        path will be put. The directory tree will be created in case it
        doesn't exist.
    use_passive_mode: bool, optional
        Whether to use FTP PASSIVE mode.
    netrc_path: str, optional
        Full path to the netrc file that has the configuration for logging in
        to the designated host. Defaults to ``~/.netrc``

    Returns
    -------
    bool
        Overall result of the operation.

    """

    parsed_url = urlparse(url)
    destination = parsed_url.path
    destination_dir = os.path.dirname(destination)
    with get_ftp_connection(parsed_url.hostname, port=parsed_url.port or 21,
                            use_passive_mode=use_passive_mode,
                            netrc_path=netrc_path) as ftp_host:
        logger.debug(
            "Sending {!r} to {!r}...".format(
                path, os.path.join(path, destination))
        )
        ftp_host.makedirs(destination_dir)
        ftp_host.upload(path, destination)
    return True


@contextmanager
def get_ftp_connection(host, port=21, use_passive_mode=False,
                       netrc_path=None):
    ftp_conf = netrc(netrc_path)
    try:
        user, password = ftp_conf.hosts[host][0:3:2]
    except KeyError:
        raise RuntimeError(
            "Host {!r} is not configured in the netrc file".format(host))
    factory = session_factory(
        port=port, use_passive_mode=use_passive_mode)
    with ftputil.FTPHost(host, user, password,
                         session_factory=factory) as ftp_host:
        yield ftp_host
