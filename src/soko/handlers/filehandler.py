# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""handlers for URLs using the `file` scheme."""

from future.standard_library import install_aliases
install_aliases()

# the following comments that start with noqa instruct flake8 to ignore the
# import error that is caused by using the `future` module in order to
# provide code compatible with python2

import logging  # noqa: E402
import os  # noqa: E402
import shutil  # noqa: E402
import re  # noqa: E402
from urllib.parse import urlparse  # noqa: E402

logger = logging.getLogger(__name__)


def get_resource(url, destination=None, **kwargs):
    """Fetch a URL from the local filesystem.

    Parameters
    ----------
    url: str
        file URL with the address of the path to get from the local host
    destination: str
        Path to a local directory where the resource returned by the URL is
        to be copied to.

    Returns
    -------
    str
        Local path to the newly fetched file or None if the resource could not
        be gotten.

    """

    found = list_resource(url)
    if found is not None:
        destination = destination or os.getcwd()
        if not os.path.isdir(destination):
            try:
                os.makedirs(destination)
            except OSError as err:
                if err.args[0] == 17:  # the directory already exists
                    pass
        try:
            parsed_found = urlparse(found)
            shutil.copy(parsed_found.path, destination)
        except IOError as exc:
            logger.debug(exc)
            result = None
        else:
            result = os.path.join(
                destination, os.path.basename(parsed_found.path))
    else:
        result = None
    return result


def list_resource(url, **kwargs):
    """Look for a resource

    Parameters
    ----------
    url: str
        URL where the resource is to be searched. This URL is interpreted as
        a regular expression pattern and matched against all resources
        available. The first matched resource is returned.

    Returns
    -------
    str
        URL of the found resource or None

    """

    parsed_url = urlparse(url)
    directory, file_name_pattern = os.path.split(parsed_url.path)
    pattern = re.compile(file_name_pattern)
    if os.path.isdir(directory):
        for item in os.listdir(directory):
            item_path = os.path.join(directory, item)
            if pattern.search(item) is not None and os.path.isfile(item_path):
                result = "file://{}".format(item_path)
                break
        else:
            result = None
    else:
        result = None
    return result


def put_resource(source, destination, delete_original=False, **kwargs):
    parsed_source_url = urlparse(source)
    parsed_destination_url = urlparse(destination)
    destination = parsed_destination_url.path
    destination_dir, destination_name = os.path.split(destination)
    try:
        os.makedirs(destination_dir)
    except Exception:  # directory already exists
        pass
    try:
        shutil_op = shutil.move if delete_original else shutil.copy
        shutil_op(parsed_source_url.path, destination)
    except (OSError, IOError):
        os.removedirs(destination_dir)
        raise
    return destination
