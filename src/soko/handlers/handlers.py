# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""handlers for resource urls"""

from future.standard_library import install_aliases
install_aliases()

# the following comments that start with noqa instruct flake8 to ignore the
# import error that is caused by using the `future` module in order to
# provide code compatible with python2

import logging  # noqa: E402
from urllib.parse import urlparse  # noqa: E402

from . import filehandler  # noqa: E402
from . import ftphandler  # noqa: E402
from . import httphandler  # noqa: E402
from . import sftphandler  # noqa: E402

logger = logging.getLogger(__name__)


def get_url(url, destination, **general_handler_config):
    handler, conf = _get_handler(
        urlparse(url).scheme,
        "get",
        configuration=general_handler_config
    )
    return handler(url, destination=destination, **conf)


def list_url(url, **general_handler_config):
    handler, conf = _get_handler(
        urlparse(url).scheme,
        "list",
        general_handler_config
    )
    return handler(url, **conf)


def put_url(source, destination, **general_handler_config):
    """Send the source to destination.

    Parameters
    ----------
    source: str
        A URL with the source that is to be sent
    destination: str
        A URL with the destination to send the resource to
    general_handler_config: dict, optional
        A mapping with the url's scheme as key and additional keyword
        arguments to pass down to the protocol handler

    """

    handler, conf = _get_handler(
        urlparse(destination).scheme,
        "put",
        general_handler_config
    )
    return handler(source, destination, **conf)


# TODO: Load the handler modules in a lazy fashion
def _get_handler(url_scheme, operation, configuration=None):
    scheme = url_scheme if url_scheme != "" else "file"
    handler = {
        "file": {
            "get": filehandler.get_resource,
            "list": filehandler.list_resource,
            "put": filehandler.put_resource,
        },
        "sftp": {
            "get": sftphandler.get_resource,
            "list": sftphandler.list_resource,
            "put": sftphandler.put_resource,
        },
        "ftp": {
            "get": ftphandler.get_resource,
            "list": ftphandler.list_resource,
            "put": ftphandler.put_resource,
        },
        "http": {
            "get": httphandler.get_resource,
            "list": httphandler.list_resource,
            "put": httphandler.put_resource,
        }
    }[scheme][operation]
    conf = dict(configuration) if configuration is not None else {}
    handler_config = conf.get(scheme, {})
    return handler, handler_config
