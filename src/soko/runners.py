# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Package runners for soko

Intended execution example:

>>> import os
>>> from soko.utilities import load_settings
>>> from soko.packages import get_package
>>> settings = load_settings(os.getenv("SOKO_SETTINGS"))
>>> package = get_package("my_package", settings=settings, timeslot="20170101")
>>> run_process(package)
>>> run_clean(package)
>>> run_send(package)

"""

import logging
from shutil import rmtree

from backports.tempfile import mkdtemp
from backports.tempfile import TemporaryDirectory

logger = logging.getLogger(__name__)


def run_process(package, force_retrieve=False,
                restrict_retrieval_schemes=None,
                output_dir=None,
                scratch_dir=None, cache_dir=None,
                move_outputs_to_final_destination=True,
                remove_output_dir=True):
    """Run a soko process.

    A process consists in the sequential execution of a package's methods
    in order to reach a result.

    Parameters
    ----------
    package: soko.packages.BasePackage
    force_retrieve: bool, optional
        Whether the package's inputs are to be copied from their original
        locations or only listed
    restrict_retrieval_schemes: list, optional
        Whether to use only certain URL schemes when trying to retrieve
        package inputs. This can be useful for retrieving only from local
        sources, for example. The default value of None means to use all of
        the configured schemes.
    output_dir: str, optional
        An intermediary directory where the outputs will be created before
        sending them to their defined destinations. The default value of
        ``None`` causes the outputs to be generated inside a new temporary
        directory. This directory may or may not be deleted at the end of
        processing, depending on the value of the ``remove_output_dir``
        parameter
    scratch_dir: str, optional
        A directory for storing temporary files that may originate from the
        processing. The default value of ``None`` means that a temporary
        directory will be created and deleted after execution. Any other value
        means that the specified directory is to be used. In this case, the
        scratch directory is assumed to be managed by the calling process so
        no creation or deletion will happen.
    cache_dir: str, optional
        A directory for storing files that are to be reused between
        consecutive runs. this directory is not managed by soko at all. It is
        assumed to already exist and it is never removed
    move_outputs_to_final_destination: bool, optional
        Whether the outputs are to be sent to their configured destinations. A
        value of ``False`` means that the outputs will be left in the
        ``output_dir`` directory.
    remove_output_dir: bool, optional
        Whether the output_dir should be removed after processing is finished.

    """

    out_dir = output_dir if output_dir is not None else mkdtemp()
    logger.debug("out_dir: {}".format(out_dir))
    with TemporaryDirectory() as auto_scratch_dir:
        scratch = scratch_dir if scratch_dir is not None else auto_scratch_dir
        retrieved = package.retrieve_inputs(
            destination=scratch, force=force_retrieve,
            allowed_schemes=restrict_retrieval_schemes
        )
        pre_result = package.perform_pre_execution(
            retrieved, output_dir=scratch, cache_dir=cache_dir)
        main_result = package.execute(
            retrieved, output_dir=out_dir, scratch_dir=scratch,
            cache_dir=cache_dir, pre_execution_result=pre_result
        )
        package.perform_post_execution(
            main_result, pre_execution_result=pre_result,
            output_dir=scratch_dir, cache_dir=cache_dir
        )
        if move_outputs_to_final_destination:
            package.move_outputs_to_final_destination(out_dir)
        if remove_output_dir:
            rmtree(out_dir)
    return True


def run_clean(package, clean_outputs_dir=True):
    # remove scratch_dir and outputs_dir, assuming outputs have been
    # already copied to their final destination
    raise NotImplementedError


def run_send(package):
    # get output_roots
    # send package outputs to output_roots
    raise NotImplementedError
