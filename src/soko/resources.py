# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import re

from jinja2.environment import Environment

from . import utilities

LOGGER = logging.getLogger(__name__)


def get_resources(name, settings=None, **kwargs):
    """Create resources.

    Parameters
    ----------
    name: str
        Name of the resource specification to use for instantiating resources.
        Must match one of the keys of the input `settings` object
    settings: dict
        A mapping with the previously read resource settings

    Returns
    -------
    list
        An iterable with the resources that were created


    Examples
    --------

    >>> from soko import utilities
    >>> settings = utilities.load_settings("soko.config.yml")
    >>> get_resources("my_resource", settings=settings["resources"])

    """

    settings = settings.copy() if settings is not None else {}
    context = settings.get("context", {})
    context.update(kwargs)
    jinja_environment = context.get("jinja_environment", Environment())
    resource_settings = settings.get("resources", {})[name]
    repetitions = resource_settings.get("repetitions")
    resources = []
    initialization_params = {
        "name": name,
        "name_patterns": resource_settings["name_patterns"],
        "uri_pattern": resource_settings.get("uri_pattern"),
        "input_search_url_patterns": resource_settings[
            "input_search_url_patterns"],
        "output_search_url_patterns": resource_settings.get(
            "output_search_url_patterns"),
        "alternatives": resource_settings.get("alternatives"),
        "jinja_environment": jinja_environment,
    }
    initialization_params.update(context)
    if repetitions is not None:
        for combination in utilities.combine_entries(repetitions):
            initialization_params.update(combination)
            resource = Resource(**initialization_params)
            resources.append(resource)
    else:
        resources.append(
            Resource(**initialization_params)
        )
    return resources


class Resource(object):
    """Resources used in the context of a processing system.

    A resource is any piece of information that is used in the system.

    Instances of this class are usually created by calling the
    ``get_resources()`` function.

    Parameters
    ----------
    name: str
        An identifier for the resource.
    name_patterns: list
        An iterable with strings that are used as patterns for the resource's
        filename.
    search_url_patterns: list
        An iterable with strings that are used as URL patterns used when
        retrieving the resource
    uri_pattern: str, optional
        An uri for the resource's that can be used to uniquely identify it.
        If not specified, it defaults to the same value as the `name`
        parameter
    jinja_environment: jinja2.environment.Environment, optional
        The environment used when the instance's pattern based properties are
        rendered
    alternatives: dict, optional
        A mapping where the keys are additional pattern marks and the values
        are used when rendering

    """

    jinja_environment = None
    _alternatives = dict()
    name = ""
    _search_urls = []
    _input_search_url_patterns = []
    _output_search_url_patterns = []
    _name_patterns = []
    _uri_pattern = ""

    def __init__(self, name, name_patterns, input_search_url_patterns,
                 output_search_url_patterns=None,
                 uri_pattern=None, jinja_environment=None, alternatives=None,
                 **kwargs):
        self.name = name
        self._name_patterns = list(name_patterns)
        self._input_search_url_patterns = list(input_search_url_patterns)
        self._output_search_url_patterns = (
            list(output_search_url_patterns) if
            output_search_url_patterns is not None else
            self._input_search_url_patterns
        )
        self._uri_pattern = uri_pattern or self.name
        self.jinja_environment = jinja_environment or Environment()
        self._alternatives = (
            alternatives.copy() if alternatives is not None else dict())
        self.__dict__.update(kwargs)

    def __str__(self):
        return self.long_name

    @property
    def long_name(self):
        long_names = set()
        for name_pattern in self._name_patterns:
            long_name = self.name
            for param in _extract_jinja_marks(name_pattern):
                try:
                    value = getattr(self, param)
                    long_name = "_".join((long_name, param, value))
                except AttributeError:
                    pass
            long_names.add(long_name)
        return max(long_names)

    def get_name_patterns(self, **kwargs):
        patterns = set()
        combined_alternatives = utilities.combine_entries(self._alternatives)
        for name_pattern in self._name_patterns:
            template = self.jinja_environment.from_string(name_pattern)
            for alternative in combined_alternatives:
                context = dict(self.__dict__)
                context.update(alternative.copy())
                context.update(kwargs)
                patterns.add(template.render(**context))
        return list(patterns)

    def get_input_search_urls(self, data_roots=None, **kwargs):
        roots = list(data_roots) if data_roots is not None else []
        return self._get_search_urls(
            data_roots=roots,
            search_url_patterns=self._input_search_url_patterns,
            **kwargs
        )

    def get_output_search_urls(self, data_roots=None, **kwargs):
        roots = list(data_roots) if data_roots is not None else []
        return self._get_search_urls(
            data_roots=roots,
            search_url_patterns=self._output_search_url_patterns,
            **kwargs
        )

    def get_uri(self, **kwargs):
        template = self.jinja_environment.from_string(self._uri_pattern)
        context = dict(self.__dict__)
        context.update(kwargs)
        return template.render(**context)

    def _get_search_urls(self, data_roots, search_url_patterns, **kwargs):
        combined_alternatives = utilities.combine_entries(self._alternatives)
        if any(data_roots):
            search_urls = set()
            for data_root in data_roots:
                root_urls = self._get_search_urls_single_root(
                    data_root=data_root,
                    combined_alternatives=combined_alternatives,
                    search_url_patterns=search_url_patterns,
                    **kwargs
                )
                search_urls.update(root_urls)
        else:  # not using data roots
            search_urls = self._get_search_urls_single_root(
                data_root="",
                combined_alternatives=combined_alternatives,
                search_url_patterns=search_url_patterns,
                **kwargs
            )
        urls = list(search_urls)
        urls.sort(key=lambda url: 0 if url.startswith("file") else 1)
        return urls

    def _sort_search_urls(self, search_urls):
        search_urls.sort(key=lambda url: 1 if url.startswith("file://") else 0)

    def _get_search_urls_single_root(self, data_root, combined_alternatives,
                                     search_url_patterns, **kwargs):
        urls = set()
        for search_pattern in search_url_patterns:
            template = self.jinja_environment.from_string(search_pattern)
            for alternative in combined_alternatives:
                for name_pattern in self.get_name_patterns(**kwargs):
                    context = dict(self.__dict__)
                    context.update(alternative.copy())
                    context["name_pattern"] = name_pattern
                    context["DATA_ROOT"] = data_root
                    context.update(kwargs.copy())
                    urls.add(template.render(**context))
        return urls


def _extract_jinja_marks(pattern):
    """Extract template marks from a Jinja pattern.

    Parameters
    ----------
    pattern: str
        The Jinja pattern to extract marks from

    Returns
    -------
    list
        The extracted marks

    Examples
    --------
    >>> _extract_jinja_marks('some_{{ name }}_pattern_with_{{ age }}_marks')
    ['name', 'age']

    """

    mark_pattern = r"{{(.*?)}}"
    return [mark.strip() for mark in re.findall(mark_pattern, pattern)]
