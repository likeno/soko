# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Custom jinja2 filters."""


def modern_format(value, format_str):
    """A custom Jinja filter for formatting stuff.

    This is useful for allowing formats on datetime.datetime objects, as the
    default Jinja ``format`` filter does not work correctly.

    """

    try:
        result = format(value, format_str)
    except TypeError:
        result = value
    except ValueError:
        result = ""
    return result


def version_format(value, minor=False, patch=False, pre=False):
    """Custom jinja filter to format semver version strings"""
    numbers_plus_meta, extra = value.partition("-")[::2]
    if extra:
        pre_release, build_metadata = extra.partition("+")[::2]
        version_numbers = numbers_plus_meta
    else:
        pre_release = None
        version_numbers, build_metadata = numbers_plus_meta.partition("+")[::2]
    major_version, minor_version, patch_version = version_numbers.split(".")
    result = major_version
    if minor:
        result = ".".join((major_version, minor_version))
        if patch:
            result = ".".join((major_version, minor_version, patch_version))
            if pre:
                result = result + "-" + pre_release if pre_release else result
    return result
