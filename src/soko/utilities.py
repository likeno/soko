# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""This module holds some utility functions for the giosystem package.

In order to use logging inside any function defined here, remember to call
the _get_logger function() first.

"""

from importlib import import_module
from itertools import product
import logging
import os
import pkgutil
import sys

from jinja2 import Environment
import yaml

from .configvalidator import validate_resource_settings
from . import filters


logger = logging.getLogger(__name__)


def combine_entries(parameters):
    """combine entries in order to ease usage in code.

    Each parameter's values are combined into a list

    Parameters
    ----------
    parameters: dict
        A mapping in which the values are iterables

    Returns
    -------
    list
        A list of dicts with the combined parameters

    Examples
    --------
    >>> combine_entries({"a": [1, 2], "b": [3, 4]})
    [{"a": 1, "b": 3}, {"a": 1, "b": 4}, {"a": 2, "b": 3}, {"a": 2, "b": 4}]

    """

    combination_lists = []
    for name, values in parameters.items():
        combination_lists.append(product([name], values))
    combined_lists = product(*combination_lists)
    result = []
    for item in combined_lists:
        result.append({param[0]: param[1] for param in item})
    return result


def get_roots(environment):
    """Return the roots for using in the system.

    A data root represents the base URL for any given resource.

    Parameters
    ----------
    environment: dict
        A mapping with the current environment variables

    Returns
    -------
    dict
        A mapping with the input and output data roots that have been specified
        through the environment

    Examples
    --------
    >>> import os
    >>> get_roots(os.environ)
    {'input': ["file://r1"], 'output': ["file://r2"], 'home': "/home/user"}

    """

    env = dict(environment)
    input_roots = env.get("SOKO_INPUT_ROOTS", "").split()
    output_roots = env.get(
        "SOKO_OUTPUT_ROOTS", "").split() or input_roots
    parsed_roots = {
        "SOKO_HOME": os.path.expanduser(env.get("SOKO_HOME", "~")),
        "SOKO_INPUT_ROOTS": input_roots,
        "SOKO_OUTPUT_ROOTS": output_roots,
    }
    return parsed_roots


def lazy_import(path):
    """Import a python named object dynamically.

    Parameters
    ----------
    path: str
        Path to import. It can be one of:
        * A python path, like soko.packages.PythonCallbackPackage
        * A filesystem path, with the indication of the python name to import
          being given using a colon, like
          ~/processes/callbacks.py:process_stuff

    """

    try:
        python_name = _lazy_import_filesystem_path(path)
    except (RuntimeError, KeyError):
        python_name = _lazy_import_python_path(path)
    return python_name


def load_settings(settings_path=None, environment=None):
    """Load resource settings from a YAML configuration file.

    Settings are searched for in the following places:

    - The ``settings_path`` parameter
    - An environment variable named SOKO_SETTINGS parameter

    Settings are loaded and validated for errors.

    Parameters
    ----------
    settings_path: str, optional
        Path to the resource settings YAML file. It can be provided in one
        of two ways:

        * a filesystem path, in which case it must be an absolute path
        * package dependent path, specifying the name of an installed python
          package and the path to the settings file relative to this package.
          This is useful for projects that inherit from soko.
    environment: dict, optional
        Environment variables to use and search for the SOKO_SETTINGS variable

    Returns
    -------
    dict
        The loaded settings

    """

    if settings_path is not None:
        path = settings_path
    elif environment is not None:
        path = environment.get("SOKO_SETTINGS")
    else:
        path = None
    if path is None:
        raise RuntimeError(
            "No settings path provided. Provide an explicit settings_path or "
            "set the 'SOKO_SETTINGS' environment variable"
        )
    elif path.startswith("/"):
        config = _load_settings_from_path(path)
    else:
        config = _load_settings_with_pkgutil(path)
    validate_resource_settings(config)
    context = generate_context()
    if "context" not in config.keys():
        config["context"] = context.copy()
    else:
        config["context"].update(context)
    config["context"].setdefault("resource_handlers_config", {})
    return config


def generate_context():
    jinja_environment = Environment()
    jinja_environment.filters["format"] = filters.modern_format
    jinja_environment.filters["version_format"] = filters.version_format
    context = {
        "jinja_environment": jinja_environment,
    }
    context.update(get_roots(os.environ))
    return context


def _lazy_import_filesystem_path(path):
    """Lazily import a python name from a filesystem path

    Parameters
    ----------
    path: str
        A colon separated string with the path to the module to load and the
        name of the object to import

    Returns
    -------
    The imported object

    Raises
    ------
    KeyError
        If the name is not found on the loaded python module
    RuntimeError
        If the path is not valid

    """

    filesystem_path, python_name = path.rpartition(":")[::2]
    full_path = os.path.abspath(os.path.expanduser(filesystem_path))
    if os.path.isfile(full_path):
        dirname, filename = os.path.split(full_path)
        module_name = os.path.splitext(filename)[0]
        sys.path.append(dirname)
        loaded_module = import_module(module_name)
        return loaded_module.__dict__[python_name]
    else:
        raise RuntimeError("Invalid path {!r}".format(full_path))


def _lazy_import_python_path(path):
    module_path, python_name = path.rpartition(".")[::2]
    loaded_module = import_module(module_path)
    return loaded_module.__dict__.get(python_name)


def _load_settings_with_pkgutil(settings_info):
    package, path = settings_info.partition(".")[::2]
    config = yaml.safe_load(
        pkgutil.get_data(package, path))
    logger.debug("Loaded settings {!r} from environment".format(settings_info))
    return config


def _load_settings_from_path(path):
    with open(path) as fh:
        config = yaml.safe_load(fh)
    return config


def render_callback_template_arg(arg_name, callback_kwargs):
    """Since the addition of the 'callback_arguments'
    directive to the .yml settings file, template arguments
    can be added to be rendered inside the callback
    functions.
    Use this function inside the callback process to ease
    render template strings (callback_arguments) with the
    respective package context.
    """
    package = callback_kwargs['package']
    env = package.jinja_environment
    context = dict(package.__dict__)
    context.update(callback_kwargs)
    return env.from_string(
        callback_kwargs[arg_name]
    ).render(context)
