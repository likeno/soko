# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Custom types for easier specification of a temporal multiplicity rule for
defining package members."""

from __future__ import division

from calendar import monthrange
import datetime as dt
from itertools import count
import logging

import dateutil.parser

LOGGER = logging.getLogger(__name__)


def get_timeslot(raw_timeslot):
    """Convert between a string and a datetime, if needed."""
    if isinstance(raw_timeslot, dt.datetime):
        result = raw_timeslot
    elif isinstance(raw_timeslot, dt.date):
        result = dt.datetime(raw_timeslot.year, raw_timeslot.month,
                             raw_timeslot.day)
    else:
        result = dateutil.parser.parse(raw_timeslot)
    return result


def transform_timeslot(original_timeslot, offset_years=0, offset_months=0,
                       offset_days=0, offset_hours=0, offset_minutes=0,
                       offset_dekades=0):
    """Return a new timeslot based on the input displacement rules.

    Parameters
    ----------
    original_timeslot: datetime.datetime or date or str
        the timeslot to offset
    offset_years: int, optional
        Number of years to offset the timeslot
    offset_months: int, optional
        Number of months to offset the timeslot
    offset_days: int, optional
        Number of days to offset the timeslot
    offset_hours: int, optional
        Number of hours to offset the timeslot
    offset_minutes: int, optional
        Number of minutes to offset the timeslot

    offset_dekades: int, optional
        Number of dekades to offset the timeslot.

        There are three decades in each month, starting at the
        1st, 11th and 21st days. The third decade's length depends
        on the number of days in the month.

        The default value is None. If it is set 0, The timeslot
        is reduced to the 1st day of the current decade.

    Returns
    -------
    datetime.datetime
        A new timeslot transformed by the input offsets.

    """

    timeslot = get_timeslot(original_timeslot)
    offset_by_years = _offset_timeslot_by_years(timeslot, offset_years)
    offset_by_years_months = _offset_timeslot_by_months(offset_by_years,
                                                        offset_months)
    offset_by_years_months_dekades = _offset_timeslot_by_dekades(
        offset_by_years_months, offset_dekades)
    result = (
        offset_by_years_months_dekades +
        dt.timedelta(days=offset_days,
                     hours=offset_hours,
                     minutes=offset_minutes)
    )
    return result


class TimeslotCalculator(object):
    """Create multiple timeslots.

    Each `start_*` and `stop_*` parameter is relative to a reference timeslot.

    It is assumed that each of the `start` attributes represent a point in
    time that is *before* its corresponding `end` attribute. Examples:

    * reference year: 2015
      start year: 1
      end year: 3
      This will generate slots for 2016, 2017, 2018

    * reference year: 2015
      start year: -1
      end year: 3
      This will generate slots for 2014, 2015, 2016, 2017, 2018

    * reference year: 2015
      start year: -3
      end year: -1
      This will generate slots for 2012, 2013, 2014

    * reference year: 2015
      start year: -1
      end year: -3
      This will generate an error because the start_year refers to a period
      in time that is after the end_year

    Parameters
    ----------
    start_year
    start_month
    start_day
    start_hour
    start_minute
    end_year
    end_month
    end_day
    end_hour
    end_minute
    frequency_year
    frequency_month
    frequency_day
    frequency_hour
    frequency_minute
    """

    def __init__(self, start_year=0, end_year=0, frequency_year=0,
                 start_month=0, end_month=0, frequency_month=0,
                 start_day=0, end_day=0, frequency_day=0,
                 start_hour=0, end_hour=0, frequency_hour=0,
                 start_minute=0, end_minute=0, frequency_minute=0,
                 start_dekade=0, end_dekade=0, frequency_dekade=0,
                 fix_hour=False):
        self.fix_hour = fix_hour
        self.start_year = start_year
        self.end_year = end_year
        self.frequency_year = frequency_year
        self.start_month = start_month
        self.end_month = end_month
        self.frequency_month = frequency_month
        self.start_day = start_day
        self.end_day = end_day
        self.frequency_day = frequency_day
        self.start_hour = start_hour
        self.end_hour = end_hour
        self.frequency_hour = frequency_hour
        self.start_minute = start_minute
        self.end_minute = end_minute
        self.frequency_minute = frequency_minute
        self.start_dekade = start_dekade
        self.end_dekade = end_dekade
        self.frequency_dekade = frequency_dekade

    def get_timeslots(self, reference):
        start_slot, end_slot = self._get_bounds_slots(reference)
        slots = [start_slot]
        if self.fix_hour:
            LOGGER.debug("Hour is fixed")
            hour_frequency = 24
        else:
            hour_frequency = self.frequency_hour
        if any([self.frequency_year, self.frequency_month, self.frequency_day,
                hour_frequency, self.frequency_minute, self.frequency_dekade]):
            current = start_slot
            for _ in count(start=1):
                current = transform_timeslot(
                    current,
                    offset_years=self.frequency_year,
                    offset_months=self.frequency_month,
                    offset_days=self.frequency_day,
                    offset_hours=hour_frequency,
                    offset_minutes=self.frequency_minute,
                    offset_dekades=self.frequency_dekade
                )
                if current > end_slot:
                    break
                else:
                    slots.append(current)
        else:
            pass  # no need to iterate, start_slot is the only one
        return slots

    def _get_bounds_slots(self, reference):
        result = []
        for bound in ("start", "end"):
            result.append(
                transform_timeslot(
                    reference,
                    self.__dict__["{}_year".format(bound)],
                    self.__dict__["{}_month".format(bound)],
                    self.__dict__["{}_day".format(bound)],
                    self.__dict__["{}_hour".format(bound)],
                    self.__dict__["{}_minute".format(bound)],
                    self.__dict__["{}_dekade".format(bound)],
                )
            )
        return result


def _offset_timeslot_by_dekades(original_timeslot, offset_dekades=0):
    result = original_timeslot
    current_dekade_advancement = 0
    if offset_dekades != 0:
        offset_factor = int(offset_dekades / abs(offset_dekades))  # 1 or -1
        while current_dekade_advancement < abs(offset_dekades):
            if offset_factor == -1:
                offset_days = 10
            else:
                if result.day < 21:
                    # 1st and 2nd dekades are always 10 days long
                    offset_days = 10
                else:
                    # 3rd dekade's length varies
                    days_in_the_month = monthrange(
                        result.year, result.month)[1]
                    offset_days = days_in_the_month - 20  # days in dekade
            result += dt.timedelta(days=offset_days * offset_factor)
            current_dekade_advancement += 1
    return result


def _offset_timeslot_by_years(original_timeslot, offset_years=0):
    new_year = original_timeslot.year + offset_years
    try:
        result = original_timeslot.replace(year=new_year)
    except ValueError:
        LOGGER.warning(
            "Also changing original day to the day before due to leap year")
        result = original_timeslot.replace(year=new_year,
                                           day=original_timeslot.day - 1)
    return result


def _offset_timeslot_by_months(original_timeslot, offset_months=0):
    years_to_offset = int(round(offset_months / 12))
    offset_by_years = _offset_timeslot_by_years(original_timeslot,
                                                years_to_offset)
    months_to_offset = abs(offset_months) % 12
    if months_to_offset == 0:
        result = offset_by_years
    else:
        offset_factor = int(offset_months / abs(offset_months))  # 1 or -1
        new_month = (offset_by_years.month +
                     offset_factor * months_to_offset) % 12
        goes_into_previous_year = (offset_by_years.month < new_month and
                                   offset_factor == -1)
        goes_into_next_year = (offset_by_years.month > new_month and
                               offset_factor == 1)
        year_factor = (-1 if goes_into_previous_year else 1 if
                       goes_into_next_year else 0)
        new_year = offset_by_years.year + year_factor
        try:
            result = offset_by_years.replace(year=new_year,
                                             month=new_month)
        except ValueError:
            LOGGER.warning(
                "Also changing original day to the last day of the month")
            last_day_of_month = monthrange(new_year, new_month)[1]
            result = offset_by_years.replace(year=new_year, month=new_month,
                                             day=last_day_of_month)
    return result
