"""Manage soko releases

This module automates some tedious steps for releasing new versions of soko.
"""

import argparse
import logging
import os
from subprocess import call

PROJECT_ROOT = os.path.abspath(
    os.path.dirname(os.path.dirname(__file__))
)
logger = logging.getLogger(__name__)


def main(version):
    update_version_file(version)
    git_commit("Preparing to tag version {}".format(version))
    create_git_tag(version)
    dev_version = _get_dev_version(version)
    update_version_file(dev_version)
    git_commit("Put version back at dev state")
    git_push("master")
    git_push("v{}".format(version))


def update_version_file(version):
    logger.debug("Updating 'VERSION' file to {}...".format(version))
    version_file_path = os.path.join(PROJECT_ROOT, "VERSION")
    with open(version_file_path, "w") as fh:
        fh.write("{}\n".format(version))


def create_git_tag(version):
    tag_name = "v{}".format(version)
    logger.debug("Creating git tag {!r}".format(tag_name))
    command = ["git", "tag", "-a", "-m", "Tagging version {}".format(version),
               tag_name]
    call(command)


def git_commit(message):
    logger.debug("Committing git changes...")
    add_command = ["git", "add", "."]
    call(add_command)
    commit_command = ["git", "commit", "-m", message]
    call(commit_command)


def git_push(ref):
    logger.debug("Pushing git changes into {!r}...".format(ref))
    command = ["git", "push", "origin", ref]
    call(command)


def _get_dev_version(version):
    major, minor, patch = version.split(".")
    new_minor = str(int(minor) + 1)
    new_patch = "0-dev"
    return "{}.{}.{}".format(major, new_minor, new_patch)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "version",
        help="Version of soko that is to be released"
    )
    parser.add_argument(
        "--verbose",
        action="store_true",
        help="Be more verbose with the output"
    )
    args = parser.parse_args()
    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.WARNING)
    main(args.version)
