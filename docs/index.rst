.. soko documentation master file, created by
   sphinx-quickstart on Thu Jun 22 16:35:38 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to soko's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


soko enables organizing processing pipelines into packages that have inputs
and outputs.


Resources
---------

Resources specify what your system uses and also what it produces.


Packages
--------

Packages are the processors of the system. They may have inputs and outputs,
collectively known as members. A package member has a resource.



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
