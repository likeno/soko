soko
====

.. image:: https://gitlab.com/ricardogsilva/soko/badges/master/build.svg
    :target: https://gitlab.com/ricardogsilva/soko/commits/master

.. image:: https://codecov.io/gl/ricardogsilva/soko/branch/master/graph/badge.svg
  :target: https://codecov.io/gl/ricardogsilva/soko


A framework for organizing your code.
soko allows the definition of resources and packages.

Installation
------------

These are still early days for soko. For now, it is available on the test pypi
server:

https://testpypi.python.org/pypi/soko


You can also clone this repository, check out a tagged release (or go with the
master branch, if you're brave) and ``pip install`` it in development mode::

    git clone https://gitlab.com/likeno/soko.git
    cd soko
    pip install --editable .


Documentation
-------------

https://likeno.gitlab.io/soko
