# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.configvalidator"""

import pytest

from soko import configvalidator

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("identifier", [
    "Resource",
    "rEsource",
    "1resource1",
    " resource1",
    "resource 1",
    "resource+1",
    "*resource1",
    "resou`rce1",
    "re'source1",
    're"source1',
    "resource:1",
    "resourc;e1",
    "resource.1",
    "-resource1",
])
def test_validate_identifier_invalid(identifier):
    with pytest.raises(RuntimeError):
        configvalidator.validate_identifier(identifier)


@pytest.mark.parametrize("identifier", [
    "resource1",
    "resource_1",
])
def test_validate_identifier_valid(identifier):
    configvalidator.validate_identifier(identifier)
