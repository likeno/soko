# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.utilities"""

from io import StringIO
import os

import mock
import pytest

from soko import utilities

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("parameters, expected", [
    (
            {"a": [1, 2], "b": [3, 4], "c": [5]},
            [
                {"a": 1, "b": 3, "c": 5},
                {"a": 1, "b": 4, "c": 5},
                {"a": 2, "b": 3, "c": 5},
                {"a": 2, "b": 4, "c": 5}
            ]
    ),
])
def test_combine_entries(parameters, expected):
    result = utilities.combine_entries(parameters)
    for expected_entry in expected:
        assert expected_entry in result


@pytest.mark.parametrize("input_roots, output_roots, soko_home, expected", [
    (
        "file://input_root/01",
        None,
        None,
        {
            "SOKO_INPUT_ROOTS": ["file://input_root/01"],
            "SOKO_OUTPUT_ROOTS": ["file://input_root/01"],
            "SOKO_HOME": "phony"
        }
    ),
    (
            "file://input_root/01",
            "file://output_root/01",
            None,
            {
                "SOKO_INPUT_ROOTS": ["file://input_root/01"],
                "SOKO_OUTPUT_ROOTS": ["file://output_root/01"],
                "SOKO_HOME": "phony"
            }
    ),
    (
            None,
            None,
            "fake",
            {
                "SOKO_INPUT_ROOTS": [],
                "SOKO_OUTPUT_ROOTS": [],
                "SOKO_HOME": "phony"
            }
    ),
])
def test_get_roots(input_roots, output_roots, soko_home, expected):
    fake_env = {
        "SOKO_INPUT_ROOTS": input_roots,
        "SOKO_OUTPUT_ROOTS": output_roots,
        "SOKO_HOME": soko_home
    }
    if input_roots is None:
        del fake_env["SOKO_INPUT_ROOTS"]
    if output_roots is None:
        del fake_env["SOKO_OUTPUT_ROOTS"]
    if soko_home is None:
        del fake_env["SOKO_HOME"]
    with mock.patch("soko.utilities.os", autospec=True) as mock_os:
        mock_os.path.expanduser.return_value = "phony"
        result = utilities.get_roots(fake_env)
        assert result == expected
        mock_os.path.expanduser.assert_called_with(soko_home or "~")


@pytest.mark.parametrize("settings_package, settings_package_path", [
    ("dummypackage", "configuration/resources_v1.yml"),
    ("dummypackage", "resources_v1"),
])
def test_load_settings_with_pkgutil(settings_package, settings_package_path):
    fake_settings_info = ".".join((settings_package, settings_package_path))
    fake_config = StringIO()
    with mock.patch("soko.utilities.pkgutil", autospec=True) as mock_pkgutil:
        mock_pkgutil.get_data.return_value = fake_config
        utilities._load_settings_with_pkgutil(fake_settings_info)
        mock_pkgutil.get_data.assert_called_with(
            settings_package, settings_package_path)


@pytest.mark.parametrize("path, python_name", [
    ("some/relative/path.py", "amodule"),
    ("/some/phony/absolute/path.py", "dummy"),
])
def test_lazy_import_filesystem_path(path, python_name):
    fake_result = "this"
    complete_path = "{}:{}".format(path, python_name)
    with mock.patch("soko.utilities.import_module",
                    autospec=True) as mock_import, \
            mock.patch("soko.utilities.os",
                       autospec=True) as mock_os:
        mock_os.path.split.return_value = os.path.split(path)
        mock_os.path.isfile.return_value = True
        mock_import.return_value.__dict__[python_name] = fake_result
        result = utilities._lazy_import_filesystem_path(complete_path)
        assert mock_import.called
        assert result == fake_result


def test_lazy_import_filesystem_path_raises():
    with pytest.raises(RuntimeError):
        utilities._lazy_import_filesystem_path("phony/path")
