# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.temporal."""

import datetime as dt

import pytest

from soko import temporal

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("raw_timeslot,expected", [
    (dt.datetime(2016, 1, 1), dt.datetime(2016, 1, 1)),
    (dt.date(2016, 1, 1), dt.datetime(2016, 1, 1)),
    ("20160101", dt.datetime(2016, 1, 1)),
    ("201601010000", dt.datetime(2016, 1, 1)),
    ("2016-01-01 00:00", dt.datetime(2016, 1, 1)),
    ("2016-01-01 00:00:00", dt.datetime(2016, 1, 1)),
    ("2016-01-01T00:00:00", dt.datetime(2016, 1, 1)),
])
def test_get_timeslot(raw_timeslot, expected):
    result = temporal.get_timeslot(raw_timeslot)
    assert result == expected


@pytest.mark.parametrize("original,offset_years,expected", [
    (dt.datetime(2016, 1, 1), 0, dt.datetime(2016, 1, 1)),
    (dt.datetime(2016, 1, 1), 2, dt.datetime(2018, 1, 1)),
    (dt.datetime(2016, 7, 31), 3, dt.datetime(2019, 7, 31)),
    (dt.datetime(2016, 2, 29), 1, dt.datetime(2017, 2, 28)),
    (dt.datetime(2016, 2, 29), 4, dt.datetime(2020, 2, 29)),
    (dt.datetime(2016, 1, 1), -2, dt.datetime(2014, 1, 1)),
    (dt.datetime(2016, 7, 31), -3, dt.datetime(2013, 7, 31)),
    (dt.datetime(2016, 2, 29), -1, dt.datetime(2015, 2, 28)),
    (dt.datetime(2016, 2, 29), -8, dt.datetime(2008, 2, 29)),
])
def test_offset_by_years(original, offset_years, expected):
    result = temporal._offset_timeslot_by_years(original, offset_years)
    assert result == expected


@pytest.mark.parametrize("original,offset_months,expected", [
    (dt.datetime(2016, 1, 1), 0, dt.datetime(2016, 1, 1)),
    (dt.datetime(2016, 1, 1), 2, dt.datetime(2016, 3, 1)),
    (dt.datetime(2016, 1, 31), 1, dt.datetime(2016, 2, 29)),
    (dt.datetime(2016, 2, 29), 12, dt.datetime(2017, 2, 28)),
    (dt.datetime(2016, 1, 1), -2, dt.datetime(2015, 11, 1)),
    (dt.datetime(2016, 11, 20), 3, dt.datetime(2017, 2, 20)),
    (dt.datetime(2016, 11, 30), 3, dt.datetime(2017, 2, 28)),
    (dt.datetime(2016, 3, 31), -1, dt.datetime(2016, 2, 29)),
    (dt.datetime(2017, 3, 31), -1, dt.datetime(2017, 2, 28)),
    (dt.datetime(2017, 3, 31), -13, dt.datetime(2016, 2, 29)),
    (dt.datetime(2017, 3, 31), -25, dt.datetime(2015, 2, 28)),
])
def test_offset_by_months(original, offset_months, expected):
    result = temporal._offset_timeslot_by_months(original, offset_months)
    assert result == expected


@pytest.mark.parametrize("original,offset_dekades,expected", [
    (dt.datetime(2016, 1, 1), 0, dt.datetime(2016, 1, 1)),
    (dt.datetime(2016, 1, 1), 1, dt.datetime(2016, 1, 11)),
    (dt.datetime(2016, 1, 11), 1, dt.datetime(2016, 1, 21)),
    (dt.datetime(2016, 1, 21), 1, dt.datetime(2016, 2, 1)),
])
def test_offset_by_dekades(original, offset_dekades, expected):
    result = temporal._offset_timeslot_by_dekades(original, offset_dekades)
    assert result == expected


@pytest.mark.parametrize(
    "slot, years, months, days, hours, minutes, dekades, expected", [
        (
                dt.datetime(2017, 2, 25, 14),
                0, 0, 0, 0, 0, 0,
                dt.datetime(2017, 2, 25, 14)
        ),
        (
                dt.datetime(2017, 2, 25, 14),
                2, 0, 0, 0, 0, 0,
                dt.datetime(2019, 2, 25, 14)
        ),
        (
            dt.datetime(2017, 2, 25, 14),
            0, 1, 0, 0, 0, 0,
            dt.datetime(2017, 3, 25, 14)
        ),
        (
                dt.datetime(2017, 2, 25, 14),
                0, 0, 10, 0, 0, 0,
                dt.datetime(2017, 3, 7, 14)
        ),
        (
                dt.datetime(2017, 2, 25, 14),
                0, 0, 0, 3, 0, 0,
                dt.datetime(2017, 2, 25, 17)
        ),
        (
                dt.datetime(2017, 2, 25, 14),
                0, 0, 0, 27, 0, 0,
                dt.datetime(2017, 2, 26, 17)
        ),
        (
                dt.datetime(2017, 2, 25, 14),
                0, 0, 0, 0, 10, 0,
                dt.datetime(2017, 2, 25, 14, 10)
        ),
        (
                dt.datetime(2017, 2, 25, 14),
                0, 0, 0, 0, 100, 0,
                dt.datetime(2017, 2, 25, 15, 40)
        ),
        (
                dt.datetime(2017, 2, 21, 14),
                0, 0, 0, 0, 0, 1,
                dt.datetime(2017, 3, 1, 14)
        ),
        (
                dt.datetime(2017, 2, 21, 14),
                0, 0, 0, 0, 0, -1,
                dt.datetime(2017, 2, 11, 14)
        ),
        (
                dt.datetime(2017, 2, 21, 14),
                0, 0, 0, 0, 0, 2,
                dt.datetime(2017, 3, 11, 14)
        ),
    ]
)
def test_transform_timeslot(slot, years, months, days, hours,
                            minutes, dekades, expected):
    result = temporal.transform_timeslot(
        slot,
        offset_years=years,
        offset_months=months,
        offset_days=days,
        offset_hours=hours,
        offset_minutes=minutes,
        offset_dekades=dekades
    )
    assert result == expected


def test_timeslot_calculator_creation():
    params = {
        "start_year": 1900,
        "frequency_year": 1,
        "end_year": 1910,
        "start_month": 2,
        "frequency_month": 1,
        "end_month": 8,
        "start_day": 19,
        "frequency_day": 1,
        "end_day": 26,
        "start_hour": 12,
        "frequency_hour": 1,
        "end_hour": 19,
        "start_minute": 1,
        "frequency_minute": 1,
        "end_minute": 24,
        "start_dekade": 1,
        "frequency_dekade": 1,
        "end_dekade": 3,
    }
    result = temporal.TimeslotCalculator(**params)
    for param, value in params.items():
        assert result.__dict__[param] == value
