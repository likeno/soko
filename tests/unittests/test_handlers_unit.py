# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.handlers.handlers"""

import mock
import pytest

from soko.handlers import handlers

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("url", [
    "file:///fake/path",
    "/another/fake/path",
    "sftp://phonyhost/yet/another/path",
    "ftp://phonyhost/more/another/path",
    "http://somehost/a/path",
])
def test_get_url(url):
    fake_destination = "phony"
    general_conf = {
        "first": "this",
        "second": "that",
    }
    specific_conf = {
        "third": "other",
        "fourth": "another",
    }
    scheme = url.partition(":")[0]
    fake_handler = mock.MagicMock()
    with mock.patch.object(handlers, "_get_handler") as mock_get_handler:
        mock_get_handler.return_value = fake_handler, specific_conf
        handlers.get_url(url, fake_destination, **general_conf)
        assert mock_get_handler.called_with(scheme, "get", general_conf)
        fake_handler.assert_called_with(
            url, destination=fake_destination, **specific_conf)


@pytest.mark.parametrize("url", [
    "file:///fake/path",
    "/another/fake/path",
    "sftp://phonyhost/yet/another/path",
    "ftp://phonyhost/more/another/path",
    "http://somehost/a/path",
])
def test_list_url(url):
    general_conf = {
        "first": "this",
        "second": "that",
    }
    specific_conf = {
        "third": "other",
        "fourth": "another",
    }
    scheme = url.partition(":")[0]
    fake_handler = mock.MagicMock()
    with mock.patch.object(handlers, "_get_handler") as mock_get_handler:
        mock_get_handler.return_value = fake_handler, specific_conf
        handlers.list_url(url, **general_conf)
        assert mock_get_handler.called_with(scheme, "list", general_conf)
        fake_handler.assert_called_with(
            url, **specific_conf)


@pytest.mark.parametrize("url", [
    "file:///fake/path",
    "/another/fake/path",
    "sftp://phonyhost/yet/another/path",
    "ftp://phonyhost/more/another/path",
    "http://somehost/a/path",
])
def test_put_url(url):
    fake_destination = "/phony"
    general_conf = {
        "first": "this",
        "second": "that",
    }
    specific_conf = {
        "third": "other",
        "fourth": "another",
    }
    scheme = url.partition(":")[0]
    fake_handler = mock.MagicMock()
    with mock.patch.object(handlers, "_get_handler") as mock_get_handler:
        mock_get_handler.return_value = fake_handler, specific_conf
        handlers.put_url(url, fake_destination, **general_conf)
        assert mock_get_handler.called_with(scheme, "put", general_conf)
        fake_handler.assert_called_with(
            url, fake_destination, **specific_conf)
