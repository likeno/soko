# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.resources"""

import pytest

from soko import resources

pytestmark = pytest.mark.unit


def test_get_resources_with_repetitions():
    fake_name = "phony"
    fake_settings = {
        "resources": {
            fake_name: {
                "repetitions": {
                    "channel": ["00_7", "03_9"],
                    "segment": ["000001", "000002"]
                },
                "alternatives": {
                    "source": ["GOES12", "GOES13"],
                },
                "name_patterns": (
                    "L-000-MSG3__-{{ source }}______-{{ channel }}_075W"
                    "-{{ segment }}___"
                    "-{{ timeslot|format('%Y%m%d%H%M') }}-__"
                ),
                "input_search_url_patterns": (
                    "{{ DATA_ROOT }}/SATELLITE_DATA/{{ name_pattern }}"),
                "uri_pattern": "cgls_{{ long_name }}",
            }
        }
    }
    result = resources.get_resources(fake_name, settings=fake_settings)
    assert len(result) == 4
    for result_resource in result:
        assert result_resource.name == fake_name


@pytest.mark.parametrize("name_patterns, expected", [
    (["something"], "phony"),
    (["something{{ stuff }}"], "phony_stuff_this"),
])
def test_resource_long_name(name_patterns, expected):
    instance = resources.Resource(
        name="phony",
        name_patterns=name_patterns,
        input_search_url_patterns=["some_url_pattern"],
        stuff="this",
    )
    assert instance.long_name == expected
