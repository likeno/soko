# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.handlers.httphandler"""

import mock
import pytest

import requests

from soko.handlers import httphandler

pytestmark = pytest.mark.unit


def test_list_resource():
    fake_url = "http://something"
    with mock.patch("soko.handlers.httphandler.requests",
                    autospec=True) as mock_requests:
        httphandler.list_resource(fake_url)
        mock_requests.head.assert_called_with(fake_url)


def test_list_resource_non_existing():
    fake_url = "http://something"
    with mock.patch("soko.handlers.httphandler.requests",
                    autospec=True) as mock_requests, \
            pytest.raises(requests.HTTPError):
        mock_requests.head.side_effect = requests.HTTPError
        httphandler.list_resource(fake_url)
