# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.runners"""

import mock
import pytest

from soko import runners

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("force_retrieve", [
    False,
    True
])
def test_run_process_retrieves_inputs(force_retrieve):
    fake_retrieved_members = "phony1"
    fake_scratch_dir = "fake"
    with mock.patch("soko.packages.BasePackage",
                    autospec=True) as mock_pack, \
            mock.patch("soko.runners.TemporaryDirectory",
                       autospec=True) as mock_temp_dir, \
            mock.patch("soko.runners.mkdtemp", autospec=True) as mock_mkdtemp:
        mock_mkdtemp.return_value = "nodir"
        mock_temp_dir.return_value.__enter__ = mock.MagicMock(
            return_value=fake_scratch_dir)
        mock_temp_dir.return_value.__exit__ = mock.MagicMock(
            return_value="that")
        pack = mock_pack.return_value
        pack.retrieve_inputs.return_value = fake_retrieved_members
        pack.perform_pre_execution.return_value = None
        pack.execute.return_value = None
        pack.perform_post_execution.return_value = None
        runners.run_process(
            package=pack,
            force_retrieve=force_retrieve,
            output_dir=None,
            scratch_dir=None,
            cache_dir=None,
            move_outputs_to_final_destination=False,
            remove_output_dir=False
        )
        pack.retrieve_inputs.assert_called_with(
            destination=fake_scratch_dir,
            force=force_retrieve,
            allowed_schemes=None
        )


def test_run_process_performs_pre_execution():
    fake_retrieved_members = "phony1"
    fake_scratch_dir = "fake"
    fake_pre_result = "phony2"
    fake_output_dir = "nothing"
    fake_cache_dir = "other"
    with mock.patch("soko.packages.BasePackage",
                    autospec=True) as mock_pack, \
            mock.patch("soko.runners.TemporaryDirectory",
                       autospec=True) as mock_temp_dir:
        mock_temp_dir.return_value.__enter__ = mock.MagicMock(
            return_value=fake_scratch_dir)
        mock_temp_dir.return_value.__exit__ = mock.MagicMock(
            return_value="that")
        pack = mock_pack.return_value
        pack.retrieve_inputs.return_value = fake_retrieved_members
        pack.perform_pre_execution.return_value = fake_pre_result
        runners.run_process(
            package=pack,
            force_retrieve=False,
            output_dir=fake_output_dir,
            scratch_dir=fake_scratch_dir,
            cache_dir=fake_cache_dir,
            move_outputs_to_final_destination=False,
            remove_output_dir=False
        )
        pack.perform_pre_execution.assert_called_with(
            fake_retrieved_members,
            output_dir=fake_scratch_dir,
            cache_dir=fake_cache_dir
        )


def test_run_process_calls_execute():
    fake_retrieved_members = "phony1"
    fake_scratch_dir = "fake"
    fake_pre_result = "phony2"
    fake_output_dir = "nothing"
    fake_cache_dir = "other"
    with mock.patch("soko.packages.BasePackage",
                    autospec=True) as mock_pack, \
            mock.patch("soko.runners.TemporaryDirectory",
                       autospec=True) as mock_temp_dir:
        mock_temp_dir.return_value.__enter__ = mock.MagicMock(
            return_value=fake_scratch_dir)
        mock_temp_dir.return_value.__exit__ = mock.MagicMock(
            return_value="that")
        pack = mock_pack.return_value
        pack.retrieve_inputs.return_value = fake_retrieved_members
        pack.perform_pre_execution.return_value = fake_pre_result
        runners.run_process(
            package=pack,
            force_retrieve=False,
            output_dir=fake_output_dir,
            scratch_dir=fake_scratch_dir,
            cache_dir=fake_cache_dir,
            move_outputs_to_final_destination=False,
            remove_output_dir=False
        )
        pack.execute.assert_called_with(
            fake_retrieved_members,
            output_dir=fake_output_dir,
            scratch_dir=fake_scratch_dir,
            cache_dir=fake_cache_dir,
            pre_execution_result=fake_pre_result
        )


def test_run_process_performs_post_execution():
    fake_scratch_dir = "fake"
    fake_pre_result = "phony2"
    fake_main_result = "phonymain"
    fake_output_dir = "nothing"
    fake_cache_dir = "other"
    with mock.patch("soko.packages.BasePackage",
                    autospec=True) as mock_pack, \
            mock.patch("soko.runners.TemporaryDirectory",
                       autospec=True) as mock_temp_dir:
        mock_temp_dir.return_value.__enter__ = mock.MagicMock(
            return_value=fake_scratch_dir)
        mock_temp_dir.return_value.__exit__ = mock.MagicMock(
            return_value="that")
        pack = mock_pack.return_value
        pack.perform_pre_execution.return_value = fake_pre_result
        pack.execute.return_value = fake_main_result
        runners.run_process(
            package=pack,
            force_retrieve=False,
            output_dir=fake_output_dir,
            scratch_dir=fake_scratch_dir,
            cache_dir=fake_cache_dir,
            move_outputs_to_final_destination=False,
            remove_output_dir=False
        )
        pack.perform_post_execution.assert_called_with(
            fake_main_result,
            pre_execution_result=fake_pre_result,
            output_dir=fake_scratch_dir,
            cache_dir=fake_cache_dir,
        )


@pytest.mark.parametrize("move_to_final", [
    True,
    False
])
def test_run_process_moves_outputs_to_final_destination(move_to_final):
    fake_scratch_dir = "fake"
    fake_output_dir = "nothing"
    fake_cache_dir = "other"
    with mock.patch("soko.packages.BasePackage",
                    autospec=True) as mock_pack, \
            mock.patch("soko.runners.TemporaryDirectory",
                       autospec=True) as mock_temp_dir:
        mock_temp_dir.return_value.__enter__ = mock.MagicMock(
            return_value=fake_scratch_dir)
        mock_temp_dir.return_value.__exit__ = mock.MagicMock(
            return_value="that")
        pack = mock_pack.return_value
        runners.run_process(
            package=pack,
            force_retrieve=False,
            output_dir=fake_output_dir,
            scratch_dir=fake_scratch_dir,
            cache_dir=fake_cache_dir,
            move_outputs_to_final_destination=move_to_final,
            remove_output_dir=False
        )
        if move_to_final:
            pack.move_outputs_to_final_destination.assert_called_with(
                fake_output_dir)
        else:
            assert pack.move_outputs_to_final_destination.call_count == 0


@pytest.mark.parametrize("remove", [
    True,
    False
])
def test_run_process_removes_output_dir(remove):
    fake_scratch_dir = "fake"
    fake_output_dir = "nothing"
    fake_cache_dir = "other"
    with mock.patch("soko.packages.BasePackage",
                    autospec=True) as mock_pack, \
            mock.patch("soko.runners.TemporaryDirectory",
                       autospec=True) as mock_temp_dir, \
            mock.patch("soko.runners.rmtree", autospec=True) as mock_rmtree:
        mock_rmtree.return_value = None
        mock_temp_dir.return_value.__enter__ = mock.MagicMock(
            return_value=fake_scratch_dir)
        mock_temp_dir.return_value.__exit__ = mock.MagicMock(
            return_value="that")
        pack = mock_pack.return_value
        runners.run_process(
            package=pack,
            force_retrieve=False,
            output_dir=fake_output_dir,
            scratch_dir=fake_scratch_dir,
            cache_dir=fake_cache_dir,
            move_outputs_to_final_destination=False,
            remove_output_dir=remove
        )
        if remove:
            mock_rmtree.assert_called_with(fake_output_dir)
        else:
            assert mock_rmtree.call_count == 0
