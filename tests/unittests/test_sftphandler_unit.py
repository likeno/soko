# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.handlers.sftphandler"""

import mock
import pytest

from soko.handlers import sftphandler

pytestmark = pytest.mark.unit


def test_get_ssh_proxy_existing_proxy():
    fake_host_config = {
        "proxycommand": "ssh someuser@somehost -W %h:%p"
    }
    mock_open = mock.mock_open()
    with mock.patch("soko.handlers.sftphandler.paramiko",
                    autospec=True) as mock_paramiko, \
            mock.patch("soko.handlers.sftphandler.open",
                       mock_open, create=True) as mock_open, \
            mock.patch.object(sftphandler,
                              "_find_host_config") as mock_find_config, \
            mock.patch("soko.handlers.sftphandler.subprocess",
                       autospec=True) as mock_subprocess:
        mock_find_config.return_value = fake_host_config
        mock_paramiko.ProxyCommand.return_value = fake_host_config[
            "proxycommand"]
        mock_subprocess.check_output.return_value = ""
        result = sftphandler._get_ssh_proxy("dummy_host")
        proxy, host_config = result
        assert proxy == fake_host_config["proxycommand"]
        assert host_config == fake_host_config


def test_get_ssh_proxy_no_proxy():
    fake_host_info = {}
    fake_openssh_config = "fake"
    host = "dummy.host"
    mock_open = mock.mock_open()
    with mock.patch("soko.handlers.sftphandler.open",
                    mock_open, create=True), \
            mock.patch("soko.handlers.sftphandler.subprocess",
                       autospec=True), \
            mock.patch("soko.handlers.sftphandler.paramiko",
                       autospec=True) as mock_paramiko:
        mock_paramiko.SSHConfig.return_value.lookup.return_value = (
            fake_host_info)
        result_proxy, result_proxy_info = sftphandler._get_ssh_proxy(
            host, openssh_config_path=fake_openssh_config)
        assert result_proxy is None
        assert result_proxy_info == fake_host_info
