# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.packages"""

import datetime as dt

import mock
import pytest

from soko import packages

pytestmark = pytest.mark.unit


def test_get_package_invalid_name():
    with pytest.raises(RuntimeError):
        packages.get_package("phony", {})


@pytest.mark.parametrize("external_command", [
    "",
    "bogus_command",
])
def test_get_package_external_command(external_command):
    fake_pack_name = "dummy"
    fake_class_path = "phony"
    fake_pack = mock.MagicMock()
    fake_settings = {
        "packages": {
            fake_pack_name: {
                "class_path": fake_class_path,
                "external_command": external_command,
            }
        }
    }
    with mock.patch("soko.packages.lazy_import", autospec=True) as mock_lazy:
        mock_lazy.return_value = fake_pack
        packages.get_package(fake_pack_name, fake_settings)
        mock_lazy.assert_called_once_with(fake_class_path)
        fake_pack.assert_called_once_with(
            name=fake_pack_name,
            settings=fake_settings,
            timeslot=None,
            pre_execution_callback=None,
            post_execution_callback=None,
            callback_arguments=None,
            external_command=external_command,
        )


def test_get_package_process_callback():
    process_callback_path = "bogus_callback_path"
    fake_pack_name = "dummy"
    fake_class_path = "phony"
    fake_pack = mock.MagicMock()
    fake_settings = {
        "packages": {
            fake_pack_name: {
                "class_path": fake_class_path,
                "process_callback": process_callback_path,
            }
        }
    }
    with mock.patch("soko.packages.lazy_import", autospec=True) as mock_lazy:
        mock_lazy.return_value = fake_pack
        packages.get_package(fake_pack_name, fake_settings)
        mock_lazy.assert_has_calls([
            mock.call(process_callback_path),
            mock.call(fake_class_path),
        ])
        fake_pack.assert_called_once_with(
            name=fake_pack_name,
            settings=fake_settings,
            timeslot=None,
            pre_execution_callback=None,
            post_execution_callback=None,
            callback_arguments=None,
            process_callback=fake_pack,
        )


@pytest.mark.parametrize("creation_kwargs", [
    {},
    {"this": "that"},
    {"this": "that", "other": "yep"},
])
def test_get_package_kwargs(creation_kwargs):
    fake_pack_name = "dummy"
    fake_class_path = "phony"
    fake_pack = mock.MagicMock()
    fake_settings = {
        "packages": {
            fake_pack_name: {
                "class_path": fake_class_path,
            }
        }
    }
    with mock.patch("soko.packages.lazy_import", autospec=True) as mock_lazy:
        mock_lazy.return_value = fake_pack
        packages.get_package(fake_pack_name, fake_settings, **creation_kwargs)
        mock_lazy.assert_called_once_with(fake_class_path)
        assert(fake_settings["context"] == creation_kwargs)
        fake_pack.assert_called_once_with(
            name=fake_pack_name,
            settings=fake_settings,
            pre_execution_callback=None,
            post_execution_callback=None,
            callback_arguments=None,
            timeslot=None,
            external_command="",
        )


def test_get_package_pre_callback():
    pre_cb_path = "bogus_callback_path"
    fake_pack_name = "dummy"
    fake_class_path = "phony"
    fake_pack = mock.MagicMock()
    fake_imported_pre_cb = mock.MagicMock()
    fake_settings = {
        "packages": {
            fake_pack_name: {
                "class_path": fake_class_path,
                "pre_execution_callback": pre_cb_path,
            }
        }
    }
    with mock.patch("soko.packages.lazy_import", autospec=True) as mock_lazy:
        mock_lazy.side_effect = [fake_imported_pre_cb, fake_pack]
        packages.get_package(fake_pack_name, fake_settings)
        mock_lazy.assert_has_calls([
            mock.call(pre_cb_path),
            mock.call(fake_class_path),
        ])
        fake_pack.assert_called_once_with(
            name=fake_pack_name,
            settings=fake_settings,
            timeslot=None,
            pre_execution_callback=fake_imported_pre_cb,
            post_execution_callback=None,
            callback_arguments=None,
            external_command="",
        )


def test_get_package_post_callback():
    post_cb_path = "bogus_callback_path"
    fake_pack_name = "dummy"
    fake_class_path = "phony"
    fake_pack = mock.MagicMock()
    fake_imported_post_cb = mock.MagicMock()
    fake_settings = {
        "packages": {
            fake_pack_name: {
                "class_path": fake_class_path,
                "post_execution_callback": post_cb_path,
            }
        }
    }
    with mock.patch("soko.packages.lazy_import", autospec=True) as mock_lazy:
        mock_lazy.side_effect = [fake_imported_post_cb, fake_pack]
        packages.get_package(fake_pack_name, fake_settings)
        mock_lazy.assert_has_calls([
            mock.call(post_cb_path),
            mock.call(fake_class_path),
        ])
        fake_pack.assert_called_once_with(
            name=fake_pack_name,
            settings=fake_settings,
            timeslot=None,
            pre_execution_callback=None,
            post_execution_callback=fake_imported_post_cb,
            callback_arguments=None,
            external_command="",
        )


@pytest.mark.parametrize("role, optional, archive, clean", [
    (packages.MemberRole.INPUT, False, False, False),
    (packages.MemberRole.INPUT, True, False, False),
    (packages.MemberRole.INPUT, False, True, False),
    (packages.MemberRole.INPUT, False, False, True),
    (packages.MemberRole.INPUT, True, True, False),
    (packages.MemberRole.INPUT, False, True, True),
    (packages.MemberRole.INPUT, True, True, True),
    (packages.MemberRole.OUTPUT, False, False, False),
    (packages.MemberRole.OUTPUT, True, False, False),
    (packages.MemberRole.OUTPUT, False, True, False),
    (packages.MemberRole.OUTPUT, False, False, True),
    (packages.MemberRole.OUTPUT, True, True, False),
    (packages.MemberRole.OUTPUT, False, True, True),
    (packages.MemberRole.OUTPUT, True, True, True),
])
def test_package_member_creation(role, optional, archive, clean):
    resource = "phony"
    slot = dt.datetime(2017, 1, 1)
    clean_offset = dt.timedelta(days=1)
    member = packages.PackageMember(
        system_resource=resource,
        role=role,
        timeslot=slot,
        optional=optional,
        should_be_archived=archive,
        should_be_cleaned=clean,
        cleaning_offset=clean_offset
    )
    assert member.role == role
    assert member.resource == resource
    assert member.timeslot == slot
    assert member.optional == optional
    assert member.should_be_archived == archive
    assert member.should_be_cleaned == clean
    assert member.cleaning_offset == clean_offset


def test_package_member_input_search_urls():
    mock_resource_class = mock.MagicMock("soko.resources.Resource")
    mock_resource = mock_resource_class.return_value
    member = packages.PackageMember(
        mock_resource, role=packages.MemberRole.INPUT)
    member.search_urls
    assert mock_resource.get_input_search_urls.called


def test_package_member_output_search_urls():
    mock_resource_class = mock.MagicMock("soko.resources.Resource")
    mock_resource = mock_resource_class.return_value
    member = packages.PackageMember(
        mock_resource, role=packages.MemberRole.OUTPUT)
    member.search_urls
    assert mock_resource.get_output_search_urls.called
