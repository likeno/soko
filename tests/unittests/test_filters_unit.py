# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Unit tests for soko.filters"""

import datetime as dt

import pytest

from soko import filters

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("value, format_str, expected", [
    (dt.datetime(2017, 1, 1, 13, 43), "%Y-%m-%d %H:%M", "2017-01-01 13:43"),
    ("something", None, "something"),
])
def test_modern_format(value, format_str, expected):
    result = filters.modern_format(value, format_str)
    assert result == expected


@pytest.mark.parametrize("version, minor, patch, pre, expected", [
    ("0.0.1", True, True, True, "0.0.1"),
    ("0.0.1-rc1", True, True, True, "0.0.1-rc1"),
    ("0.0.1-rc1+001", True, True, True, "0.0.1-rc1"),
    ("0.0.1+001", True, True, True, "0.0.1"),
    ("0.0.1-rc1+001", True, True, False, "0.0.1"),
    ("0.0.1-rc1+001", True, False, False, "0.0"),
    ("0.0.1-rc1+001", False, False, False, "0"),
    ("0.0.1-rc1+001", False, True, True, "0"),
])
def test_version_format(version, minor, patch, pre, expected):
    result = filters.version_format(
        version, minor=minor, patch=patch, pre=pre)
    assert result == expected
