# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""pytest configuration file for integration tests"""

import os

from jinja2.environment import Environment
import pytest

from soko.resources import Resource
from soko import filters
from soko import utilities

INTEGRATION_TESTS_ROOT = os.path.dirname(os.path.abspath(__file__))


@pytest.fixture()
def soko_config_path(request):
    global INTEGRATION_TESTS_ROOT
    return os.path.join(INTEGRATION_TESTS_ROOT, "config.yml")


@pytest.fixture()
def soko_config(request):
    global INTEGRATION_TESTS_ROOT
    return utilities.load_settings(
        os.path.join(INTEGRATION_TESTS_ROOT, "config.yml"))


@pytest.fixture()
def sample_resource(request):
    jinja_env = Environment()
    jinja_env.filters["format"] = filters.modern_format
    return Resource(
        "dummy",
        name_patterns=[
            ("L-000-MSG3__-{{ source }}______-{{ channel }}_075W-"
             "{{ segment }}___-{{ timeslot|format('%Y%m%d%H%M') }}-__"),
        ],
        input_search_url_patterns=[
            "{{ DATA_ROOT }}SATELLITE_DATA/{{ name_pattern }}",
        ],
        alternatives={
            "source": [
                "GOES12",
                "GOES13"
            ]
        },
        jinja_environment=jinja_env
    )
