# Copyright 2017 Ricardo Garcia Silva

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Integration tests for soko.resources"""

import pytest

from soko import resources

pytestmark = pytest.mark.integration


def test_get_resources_number_of_resources_created(soko_config):
    result = resources.get_resources(
        "raw_goes", settings=soko_config)
    assert len(result) == 24
